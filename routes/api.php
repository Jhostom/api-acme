<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Traer todos los propietarios
Route::get('propietario', 'PropietarioController@getPropietarios');

// Traer un propietario especifico
Route::get('propietario/{id}', 'PropietarioController@getPropietarioById');

// Agregar propietario
Route::post('addPropietario', 'PropietarioController@addPropietario');

// Actualizar propietario
Route::put('updatePropietario/{id}', 'PropietarioController@updatePropietario');

// Eliminar propietario
Route::delete('deletePropietario/{id}', 'PropietarioController@deletePropietario');

// Traer todos los conductores
Route::get('conductor', 'ConductorController@getConductores');

// Traer un conductor especifico
Route::get('conductor/{id}', 'ConductorController@getConductorById');

// Agregar conductor
Route::post('addConductor', 'ConductorController@addConductor');

// Actualizar conductor
Route::put('updateConductor/{id}', 'ConductorController@updateConductor');

// Eliminar conductor
Route::delete('deleteConductor/{id}', 'ConductorController@deleteConductor');

// Traer todos los vehiculos
Route::get('vehiculo', 'VehiculoController@getVehiculos');

// Traer un vehiculos especifico
Route::get('vehiculo/{id}', 'VehiculoController@getVehiculoById');

// Agregar vehiculos
Route::post('addVehiculo', 'VehiculoController@addVehiculo');

// Actualizar vehiculos
Route::put('updateVehiculo/{id}', 'VehiculoController@updateVehiculo');

// Eliminar vehiculos
Route::delete('deleteVehiculo/{id}', 'VehiculoController@deleteVehiculo');

