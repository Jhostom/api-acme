<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculo', function (Blueprint $table) {
            $table->id();
            $table->string('placa', 10)->unique();
            $table->string('color', 80);
            $table->string('marca', 200);
            $table->string('tipoVehiculo', 100);
            $table->bigInteger('conductor_id')->unsigned();
            $table->bigInteger('propietario_id')->unsigned();
            $table->timestamps();

            $table->foreign('conductor_id')->references('id')->on('conductor');
            $table->foreign('propietario_id')->references('id')->on('propietario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
}
