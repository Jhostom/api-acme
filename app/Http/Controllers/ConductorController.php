<?php

namespace App\Http\Controllers;
use App\Conductor;

use Illuminate\Http\Request;

class ConductorController extends Controller
{
    public function getConductores() {
        return response()->json(Conductor::all(), 200);
    }

    public function getConductorById($id) {
        $conductor = Conductor::find($id);
        if(is_null($conductor)) {
            return response()->json(['message' => 'Conductor no encontrado'], 404);
        }
        return response()->json($conductor::find($id), 200);
    }

    public function addConductor(Request $request) {
        $conductor = Conductor::create($request->all());
        return response($conductor, 201);
    }

    public function updateConductor(Request $request, $id) {
        $conductor = Conductor::find($id);
        if(is_null($conductor)) {
            return response()->json(['message' => 'Conductor no encontrado'], 404);
        }
        $conductor->update($request->all());
        return response($conductor, 200);
    }

    public function deleteConductor(Request $request, $id) {
        $conductor = Conductor::find($id);
        if(is_null($conductor)) {
            return response()->json(['message' => 'Conductor no encontrado'], 404);
        }
        $conductor->delete();
        return response()->json(null, 204);
    }
}
