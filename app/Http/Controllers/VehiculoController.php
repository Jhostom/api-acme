<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehiculo;
use Illuminate\Support\Facades\DB;

class VehiculoController extends Controller
{
    public function getVehiculos() {
        //return response()->json(Vehiculo::all(), 200);
        return response()->json(
        Vehiculo::join('propietario', 'propietario.id', '=', 'vehiculo.propietario_id')
        ->join('conductor', 'conductor.id', '=', 'vehiculo.conductor_id')
        ->select('vehiculo.*', DB::raw('concat(propietario.primerNombre, " ", propietario.segundoNombre, " ", propietario.apellidos ) as nombrePropietario'), DB::raw('concat(conductor.primerNombre, " ", conductor.segundoNombre, " ", conductor.apellidos ) as nombreConductor'))
        ->get()
        , 200);
    }

    public function getVehiculoById($id) {
        $vehiculo = Vehiculo::find($id);
        if(is_null($vehiculo)) {
            return response()->json(['message' => 'Vehiculo no encontrado'], 404);
        }
        return response()->json($vehiculo::join('propietario', 'propietario.id', '=', 'vehiculo.propietario_id')
        ->join('conductor', 'conductor.id', '=', 'vehiculo.conductor_id')
        ->select('vehiculo.*', DB::raw('concat(propietario.primerNombre, " ", propietario.segundoNombre, " ", propietario.apellidos ) as nombrePropietario'), DB::raw('concat(conductor.primerNombre, " ", conductor.segundoNombre, " ", conductor.apellidos ) as nombreConductor'))
        ->where('vehiculo.id', $id)
        ->first()
        , 200);
    }

    public function addVehiculo(Request $request) {
        $vehiculo = Vehiculo::create($request->all());
        return response($vehiculo, 201);
    }

    public function updateVehiculo(Request $request, $id) {
        $vehiculo = Vehiculo::find($id);
        if(is_null($vehiculo)) {
            return response()->json(['message' => 'Vehiculo no encontrado'], 404);
        }
        $vehiculo->update($request->all());
        return response($vehiculo, 200);
    }

    public function deleteVehiculo(Request $request, $id) {
        $vehiculo = Vehiculo::find($id);
        if(is_null($vehiculo)) {
            return response()->json(['message' => 'Vehiculo no encontrado'], 404);
        }
        $vehiculo->delete();
        return response()->json(null, 204);
    }
}