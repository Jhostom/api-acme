<?php

namespace App\Http\Controllers;
use App\Propietario;

use Illuminate\Http\Request;

class PropietarioController extends Controller
{
    public function getPropietarios() {
        return response()->json(Propietario::all(), 200);
    }

    public function getPropietarioById($id) {
        $propietario = Propietario::find($id);
        if(is_null($propietario)) {
            return response()->json(['message' => 'Propietario no encontrado'], 404);
        }
        return response()->json($propietario::find($id), 200);
    }

    public function addPropietario(Request $request) {
        $propietario = Propietario::create($request->all());
        return response($propietario, 201);
    }

    public function updatePropietario(Request $request, $id) {
        $propietario = Propietario::find($id);
        if(is_null($propietario)) {
            return response()->json(['message' => 'Propietario no encontrado'], 404);
        }
        $propietario->update($request->all());
        return response($propietario, 200);
    }

    public function deletePropietario(Request $request, $id) {
        $propietario = Propietario::find($id);
        if(is_null($propietario)) {
            return response()->json(['message' => 'Propietario no encontrado'], 404);
        }
        $propietario->delete();
        return response()->json(null, 204);
    }
}