<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propietario extends Model
{
    protected $table = 'propietario';

    protected $fillable = [
        'numeroCedula',
        'primerNombre',
        'segundoNombre',
        'apellidos',
        'direccion',
        'telefono',
        'ciudad'
    ];

    protected $guarded = ['id'];
}
