<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conductor extends Model
{
    protected $table = 'conductor';

    protected $fillable = [
        'numeroCedula',
        'primerNombre',
        'segundoNombre',
        'apellidos',
        'direccion',
        'telefono',
        'ciudad'
    ];

    protected $guarded = ['id'];
}