<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    protected $table = 'vehiculo';

    protected $fillable = [
        'placa',
        'color',
        'marca',
        'tipoVehiculo',
        'conductor_id',
        'propietario_id'
    ];

    protected $guarded = ['id'];

    public function conductor(){
        return $this->belongsTo('App\Conductor');
    }

    public function propietario(){
        return $this->belongsTo('App\Propietario');
    }
}